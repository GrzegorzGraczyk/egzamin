-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 21 Mar 2022, 12:37
-- Wersja serwera: 10.4.21-MariaDB
-- Wersja PHP: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `2tpe`
--
CREATE DATABASE IF NOT EXISTS `2tpe` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `2tpe`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pracownicy`
--

CREATE TABLE `pracownicy` (
  `id` int(11) NOT NULL,
  `imie` varchar(20) COLLATE utf8mb4_polish_ci NOT NULL,
  `Nazwisko` varchar(30) COLLATE utf8mb4_polish_ci NOT NULL,
  `placa` decimal(7,0) NOT NULL,
  `stanowisko` varchar(20) COLLATE utf8mb4_polish_ci NOT NULL,
  `pesel` char(11) COLLATE utf8mb4_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `pracownicy`
--

INSERT INTO `pracownicy` (`id`, `imie`, `Nazwisko`, `placa`, `stanowisko`, `pesel`) VALUES
(1, 'Adam', 'Kowalski', '1625', 'magazynier', '1234I678901'),
(2, 'Adam', 'Nowak', '3760', 'kierownik', '9234I678901'),
(3, 'Adam', 'Nowak', '2760', 'sprzedawca', '9234I678901'),
(4, 'Krzysztof', 'Malinowski', '1760', 'magazynier', '9234I678901'),
(5, 'Zygmunt', 'Nowicki', '2760', 'magazynier', '9935I677601'),
(6, 'Krzysztof', 'Nowicki', '3760', 'magazynier', '8235I678901'),
(7, 'Kamil', 'Borowski', '2360', 'asystent', '32349678913'),
(8, 'Zygmunt', 'Kwiatkowski', '2560', 'magazynier', '8935I767601'),
(9, 'Krzysztof', 'Arkuszewski', '1601', 'magazynier', '02343678913'),
(10, 'Kacper', 'Adamczyk', '1611', 'serwisant', '92341678903'),
(11, 'Arkadiusz', 'Malinowski', '1600', 'kierowca', '9234I678909'),
(12, 'Andrzej', 'Kowalski', '4200', 'kierownik', '7234I678901'),
(13, 'Kamil', 'Andrzejczak', '1200', 'asystent', ''),
(14, 'Zygmunt', 'Makówka', '2760', 'magazynier', '8854I677601'),
(15, 'Zygmunt', 'Maślanka', '2260', 'sprzedawca', '01032877601'),
(16, 'Kamila', 'Borowska', '2360', 'asystent', '99349678913'),
(17, 'Adam', 'Kowalski', '5000', 'administrator', '05272807597');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uzytkownicy`
--

CREATE TABLE `uzytkownicy` (
  `id` int(11) NOT NULL,
  `login` varchar(30) NOT NULL,
  `haslo` varchar(30) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `rejestracja` varchar(10) DEFAULT NULL,
  `logowanie` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `uzytkownicy`
--

INSERT INTO `uzytkownicy` (`id`, `login`, `haslo`, `email`, `rejestracja`, `logowanie`) VALUES
(1, '2tpeoliwierj', 'zaq12wsx', 'ojwp.pl', '05-11-2021', '05-11-2021');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `pracownicy`
--
ALTER TABLE `pracownicy`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `uzytkownicy`
--
ALTER TABLE `uzytkownicy`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `uzytkownicy`
--
ALTER TABLE `uzytkownicy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Baza danych: `auta`
--
CREATE DATABASE IF NOT EXISTS `auta` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `auta`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `autka`
--

CREATE TABLE `autka` (
  `id` int(11) NOT NULL,
  `marka` varchar(30) DEFAULT NULL,
  `model` varchar(30) DEFAULT NULL,
  `rocznik` int(11) DEFAULT NULL,
  `pojemnosc` float DEFAULT NULL,
  `przyspieszenie` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `autka`
--

INSERT INTO `autka` (`id`, `marka`, `model`, `rocznik`, `pojemnosc`, `przyspieszenie`) VALUES
(1, 'Bugatti', 'Veyron', 2018, 7993, 2.5),
(2, 'Lamborgchini', 'Aventador', 2018, 6498, 2.9);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dzial`
--

CREATE TABLE `dzial` (
  `id` int(11) NOT NULL,
  `Nazwa` varchar(30) DEFAULT NULL,
  `id_kierownika` varchar(30) DEFAULT NULL,
  `miejsce` varchar(30) DEFAULT NULL,
  `data` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kupiec`
--

CREATE TABLE `kupiec` (
  `id` int(11) NOT NULL,
  `imie` varchar(30) DEFAULT NULL,
  `nazwisko` varchar(30) DEFAULT NULL,
  `stanowisko` varchar(30) DEFAULT NULL,
  `data_zatrudnienia` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `kupiec`
--

INSERT INTO `kupiec` (`id`, `imie`, `nazwisko`, `stanowisko`, `data_zatrudnienia`) VALUES
(1, 'Maniek', 'Gangster', 'Manager', 1979),
(2, 'Jordan', 'Mafiozo', 'Manager', 1959);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `motory`
--

CREATE TABLE `motory` (
  `id` int(11) NOT NULL,
  `marka` varchar(30) DEFAULT NULL,
  `model` varchar(30) DEFAULT NULL,
  `rocznik` int(11) DEFAULT NULL,
  `cena` float DEFAULT NULL,
  `przyspieszenie` float DEFAULT NULL,
  `opis` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `motory`
--

INSERT INTO `motory` (`id`, `marka`, `model`, `rocznik`, `cena`, `przyspieszenie`, `opis`) VALUES
(1, 'Honda', 'kozak', 2018, 4000, 140, 'motorek'),
(2, 'Honda', 'kozak', 2013, 3000, 120, '2motory'),
(3, 'Kawasaki', 'kozak2', 2013, 7000, 140, '3motory'),
(4, 'Kymco', 'kozak34', 2020, 9000, 180, '4motory'),
(5, 'Benelii', 'kozak43', 2021, 14000, 220, '5motory');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pracownik`
--

CREATE TABLE `pracownik` (
  `id` int(11) NOT NULL,
  `Nazwisko` varchar(30) DEFAULT NULL,
  `Placa` int(11) DEFAULT NULL,
  `Zespol` varchar(30) DEFAULT NULL,
  `dzial` varchar(30) DEFAULT NULL,
  `data` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `pracownik`
--

INSERT INTO `pracownik` (`id`, `Nazwisko`, `Placa`, `Zespol`, `dzial`, `data`) VALUES
(1, 'Grabarczyk', 3000, 'Administracja', '3', 2021),
(2, 'Langer', 3000, 'Administracja', '2', 2021),
(3, 'Kanczel', 3000, 'Administracja', '6', 2021),
(4, 'Kleczol', 3200, 'Administracja', '4', 2021),
(5, 'Kowalski', 3100, 'Administracja', '5', 2021),
(6, 'Ranczewski', 3100, 'Administracja', '1', 2021),
(7, 'Rogaczewski', 3100, 'Administracja', '3', 2021);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `stanowiska`
--

CREATE TABLE `stanowiska` (
  `id` int(11) NOT NULL,
  `Nazwa` varchar(30) DEFAULT NULL,
  `placa` int(11) DEFAULT NULL,
  `id_dzialu` varchar(30) DEFAULT NULL,
  `data` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `autka`
--
ALTER TABLE `autka`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `dzial`
--
ALTER TABLE `dzial`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `kupiec`
--
ALTER TABLE `kupiec`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `motory`
--
ALTER TABLE `motory`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `pracownik`
--
ALTER TABLE `pracownik`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `stanowiska`
--
ALTER TABLE `stanowiska`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `autka`
--
ALTER TABLE `autka`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `dzial`
--
ALTER TABLE `dzial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `kupiec`
--
ALTER TABLE `kupiec`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `motory`
--
ALTER TABLE `motory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `pracownik`
--
ALTER TABLE `pracownik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT dla tabeli `stanowiska`
--
ALTER TABLE `stanowiska`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Baza danych: `firma`
--
CREATE DATABASE IF NOT EXISTS `firma` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `firma`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `klient`
--

CREATE TABLE `klient` (
  `id_klienta` varchar(5) DEFAULT NULL,
  `imie` varchar(20) DEFAULT NULL,
  `nazwisko` varchar(20) DEFAULT NULL,
  `miejscowosc` varchar(20) DEFAULT NULL,
  `pesel` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `klient`
--

INSERT INTO `klient` (`id_klienta`, `imie`, `nazwisko`, `miejscowosc`, `pesel`) VALUES
('1', 'Wojtek', 'Kowalski', 'Szczercow', 2147483647),
('2', 'Marta', 'Narczak', 'Szczercow', 2147483647),
('3', 'Gracjan', 'Wolczek', 'Szczercow', 2147483647),
('3', 'Stefan', 'Boloczan', 'Szczercow', 2147483647);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `towar`
--

CREATE TABLE `towar` (
  `ID_towaru` varchar(5) DEFAULT NULL,
  `nazwa` varchar(30) DEFAULT NULL,
  `cecha` varchar(30) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zamowienie`
--

CREATE TABLE `zamowienie` (
  `id_zamowienia` varchar(5) DEFAULT NULL,
  `id_klienta` varchar(5) DEFAULT NULL,
  `id_towaru` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
--
-- Baza danych: `phpmyadmin`
--
CREATE DATABASE IF NOT EXISTS `phpmyadmin` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `phpmyadmin`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pma__bookmark`
--

CREATE TABLE `pma__bookmark` (
  `id` int(10) UNSIGNED NOT NULL,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pma__central_columns`
--

CREATE TABLE `pma__central_columns` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_length` text COLLATE utf8_bin DEFAULT NULL,
  `col_collation` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_isNull` tinyint(1) NOT NULL,
  `col_extra` varchar(255) COLLATE utf8_bin DEFAULT '',
  `col_default` text COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Central list of columns';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pma__column_info`
--

CREATE TABLE `pma__column_info` (
  `id` int(5) UNSIGNED NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pma__designer_settings`
--

CREATE TABLE `pma__designer_settings` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `settings_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Settings related to Designer';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pma__export_templates`
--

CREATE TABLE `pma__export_templates` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `export_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `template_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `template_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved export templates';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pma__favorite`
--

CREATE TABLE `pma__favorite` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Favorite tables';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pma__history`
--

CREATE TABLE `pma__history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT current_timestamp(),
  `sqlquery` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pma__navigationhiding`
--

CREATE TABLE `pma__navigationhiding` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Hidden items of navigation tree';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pma__pdf_pages`
--

CREATE TABLE `pma__pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) UNSIGNED NOT NULL,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pma__recent`
--

CREATE TABLE `pma__recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';

--
-- Zrzut danych tabeli `pma__recent`
--

INSERT INTO `pma__recent` (`username`, `tables`) VALUES
('root', '[{\"db\":\"test\",\"table\":\"users\"},{\"db\":\"test\",\"table\":\"uzytkownicy\"},{\"db\":\"test\",\"table\":\"pytania\"},{\"db\":\"test\",\"table\":\"1\"},{\"db\":\"test\",\"table\":\"dvd\"},{\"db\":\"test\",\"table\":\"samochody\"},{\"db\":\"auta\",\"table\":\"autka\"},{\"db\":\"test\",\"table\":\"uprawnienia\"},{\"db\":\"pytania\",\"table\":\"pytania\"},{\"db\":\"pytania\",\"table\":\"pytanie\"}]');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pma__relation`
--

CREATE TABLE `pma__relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pma__savedsearches`
--

CREATE TABLE `pma__savedsearches` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved searches';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pma__table_coords`
--

CREATE TABLE `pma__table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT 0,
  `x` float UNSIGNED NOT NULL DEFAULT 0,
  `y` float UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pma__table_info`
--

CREATE TABLE `pma__table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pma__table_uiprefs`
--

CREATE TABLE `pma__table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';

--
-- Zrzut danych tabeli `pma__table_uiprefs`
--

INSERT INTO `pma__table_uiprefs` (`username`, `db_name`, `table_name`, `prefs`, `last_update`) VALUES
('root', 'pytania', 'pytania', '{\"sorted_col\":\"`Tresc_pytania`  ASC\"}', '2021-12-13 09:24:46'),
('root', 'test', 'uprawnienia', '{\"sorted_col\":\"`uprawnienia`.`uprawnienia` ASC\"}', '2021-10-22 10:53:16'),
('root', 'test', 'users', '{\"sorted_col\":\"`users`.`prawa`  DESC\"}', '2022-03-21 08:57:33'),
('root', 'test', 'uzytkownicy', '{\"sorted_col\":\"`uzytkownicy`.`ip1` ASC\"}', '2021-12-10 12:13:58');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pma__tracking`
--

CREATE TABLE `pma__tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin DEFAULT NULL,
  `data_sql` longtext COLLATE utf8_bin DEFAULT NULL,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database changes tracking for phpMyAdmin';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pma__userconfig`
--

CREATE TABLE `pma__userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `config_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';

--
-- Zrzut danych tabeli `pma__userconfig`
--

INSERT INTO `pma__userconfig` (`username`, `timevalue`, `config_data`) VALUES
('root', '2022-03-21 11:36:56', '{\"Console\\/Mode\":\"show\",\"lang\":\"pl\",\"DefaultConnectionCollation\":\"utf8mb4_polish_ci\",\"Console\\/Height\":253.971}');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pma__usergroups`
--

CREATE TABLE `pma__usergroups` (
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL,
  `tab` varchar(64) COLLATE utf8_bin NOT NULL,
  `allowed` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User groups with configured menu items';

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pma__users`
--

CREATE TABLE `pma__users` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and their assignments to user groups';

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `pma__central_columns`
--
ALTER TABLE `pma__central_columns`
  ADD PRIMARY KEY (`db_name`,`col_name`);

--
-- Indeksy dla tabeli `pma__column_info`
--
ALTER TABLE `pma__column_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`);

--
-- Indeksy dla tabeli `pma__designer_settings`
--
ALTER TABLE `pma__designer_settings`
  ADD PRIMARY KEY (`username`);

--
-- Indeksy dla tabeli `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_user_type_template` (`username`,`export_type`,`template_name`);

--
-- Indeksy dla tabeli `pma__favorite`
--
ALTER TABLE `pma__favorite`
  ADD PRIMARY KEY (`username`);

--
-- Indeksy dla tabeli `pma__history`
--
ALTER TABLE `pma__history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`,`db`,`table`,`timevalue`);

--
-- Indeksy dla tabeli `pma__navigationhiding`
--
ALTER TABLE `pma__navigationhiding`
  ADD PRIMARY KEY (`username`,`item_name`,`item_type`,`db_name`,`table_name`);

--
-- Indeksy dla tabeli `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  ADD PRIMARY KEY (`page_nr`),
  ADD KEY `db_name` (`db_name`);

--
-- Indeksy dla tabeli `pma__recent`
--
ALTER TABLE `pma__recent`
  ADD PRIMARY KEY (`username`);

--
-- Indeksy dla tabeli `pma__relation`
--
ALTER TABLE `pma__relation`
  ADD PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  ADD KEY `foreign_field` (`foreign_db`,`foreign_table`);

--
-- Indeksy dla tabeli `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_savedsearches_username_dbname` (`username`,`db_name`,`search_name`);

--
-- Indeksy dla tabeli `pma__table_coords`
--
ALTER TABLE `pma__table_coords`
  ADD PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`);

--
-- Indeksy dla tabeli `pma__table_info`
--
ALTER TABLE `pma__table_info`
  ADD PRIMARY KEY (`db_name`,`table_name`);

--
-- Indeksy dla tabeli `pma__table_uiprefs`
--
ALTER TABLE `pma__table_uiprefs`
  ADD PRIMARY KEY (`username`,`db_name`,`table_name`);

--
-- Indeksy dla tabeli `pma__tracking`
--
ALTER TABLE `pma__tracking`
  ADD PRIMARY KEY (`db_name`,`table_name`,`version`);

--
-- Indeksy dla tabeli `pma__userconfig`
--
ALTER TABLE `pma__userconfig`
  ADD PRIMARY KEY (`username`);

--
-- Indeksy dla tabeli `pma__usergroups`
--
ALTER TABLE `pma__usergroups`
  ADD PRIMARY KEY (`usergroup`,`tab`,`allowed`);

--
-- Indeksy dla tabeli `pma__users`
--
ALTER TABLE `pma__users`
  ADD PRIMARY KEY (`username`,`usergroup`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `pma__column_info`
--
ALTER TABLE `pma__column_info`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `pma__history`
--
ALTER TABLE `pma__history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  MODIFY `page_nr` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Baza danych: `pytania`
--
CREATE DATABASE IF NOT EXISTS `pytania` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `pytania`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pytania`
--

CREATE TABLE `pytania` (
  `ID` varchar(100) NOT NULL,
  `Tresc_pytania` varchar(1000) NOT NULL,
  `Prawidlowa_odpowiedz` varchar(100) NOT NULL,
  `A` varchar(100) NOT NULL,
  `B` varchar(100) NOT NULL,
  `C` varchar(100) NOT NULL,
  `D` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `pytania`
--

INSERT INTO `pytania` (`ID`, `Tresc_pytania`, `Prawidlowa_odpowiedz`, `A`, `B`, `C`, `D`) VALUES
('1', 'Która instr', 'A', ' n > 20', 'Wypisz n', 'n ← n + 5', 'Wykonaj pod'),
('2', 'Jak nazywa ', 'B', ' Sortowanie', 'Sortowanie ', 'Sortowanie ', 'Sortowanie '),
('3', 'Program zap', 'D', ' $ocena > 2', '$ocena > 2 ', '$ocena >= 2', '$ocena >= 2'),
('4', 'W języku C++ zdefiniowano zmienną: char zm1. W jaki sposób można do niej przypisać wartość zgodnie\r\nze składnią języka?', 'A', ' zm1 = w', 'zm1 == 0x35', 'zm1[2] = 32', 'zm1 = wiadro '),
('5', 'W języku JavaScript, aby wydzielić fragment napisu znajdujący się pomiędzy wskazanymi przez parametr indeksami należy użyć metody\r\n', 'B', ' trim()', 'slice()', 'concat()', 'replace() '),
('[6]', '[W jaki sposób, stosując język PHP, zapisać w ciasteczku napis znajdujący się w zmiennej dane na czas jednego dnia?]', '[D]', '[setcookie(\"dane\", $dane, 0);]', '[setcookie(\"dane\", \"dane\", 0);]', '[setcookie(\"dane\", $dane, time());]', '[setcookie(\"dane\", $dane, time() + (3600*24)); ]'),
('[7]', '[if (empty($_POST[\"name\"])) {\r\n$nameErr = \"Name is required\";\r\n}\r\nPrzedstawiony fragment kodu PHP służy do obsługi\r\n]', 'C', '[sesji.]', '[ciasteczek.]', '[formularza.]', '[bazy danych.]'),
('8', '[echo date(\"Y\");\r\nPo wykonaniu kodu PHP zostanie wyświetlona aktualna data zawierająca jedynie]', '[A]', '[rok.]', '[dzień.]', '[miesiąc i rok.]', '[dzień i miesiąc]'),
('9', '[Który zapis definiuje w języku PHP komentarz wieloliniowy?\r\n]', '[C]', '[#]', '[//]', '[/* */]', '[<!-- -->]'),
('10', '[Który z typów relacji wymaga utworzenia tabeli pośredniej łączącej klucze główne obu tabel?\r\n]', '[D]', '[. 1..1]', '[1..n]', '[n..1]', '[n..m]'),
('11', '[Integralność encji w bazie danych zostanie zachowana, jeżeli między innymi]', '[C]', '[klucz główny będzie zawsze liczbą całkowitą.]', '[każdej kolumnie zostanie przypisany typ danych.]', '[dla każdej tabeli zostanie utworzony klucz główny.]', '[każdy klucz główny będzie miał odpowiadający mu klucz obcy w innej tabeli.\r\n]');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pytanie`
--

CREATE TABLE `pytanie` (
  `id` int(10) UNSIGNED NOT NULL,
  `tresc` text COLLATE utf8mb4_polish_ci NOT NULL,
  `odpA` text COLLATE utf8mb4_polish_ci NOT NULL,
  `odpB` text COLLATE utf8mb4_polish_ci NOT NULL,
  `odpC` text COLLATE utf8mb4_polish_ci NOT NULL,
  `odpD` text COLLATE utf8mb4_polish_ci NOT NULL,
  `odp` text COLLATE utf8mb4_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `pytanie`
--

INSERT INTO `pytanie` (`id`, `tresc`, `odpA`, `odpB`, `odpC`, `odpD`, `odp`) VALUES
(1, 'Program zapisany w języku PHP ma za zadanie obliczyć średnią pozytywnych ocen ucznia od 2 do 6. \r\nWarunek wybierania ocen w pętli liczącej średnią powinien zawierać wyrażenie logiczne\r\n', '$ocena > 2 and $ocena < 6', '$ocena > 2 or $ocena < ', '$ocena >= 2 or $ocena <= 6', '$ocena >= 2 and $ocena <= ', 'd'),
(2, 'W jaki sposób, stosując język PHP, zapisać w ciasteczku napis znajdujący się w zmiennej dane na czas \r\njednego dnia?', 'setcookie(\"dane\", $dane, 0);', ' setcookie(\"dane\", \"dane\", 0);', ' setcookie(\"dane\", $dane, time());\r\n', ' setcookie(\"dane\", $dane, time() + (3600*24)); ', 'd'),
(3, 'if (empty($_POST[\"name\"])) {\r\n$nameErr = \"Name is required\";\r\n} \r\nPrzedstawiony fragment kodu PHP służy do obsługi\r\n', 'sesji.', 'ciasteczek.', 'formularza.', 'bazy danych.', 'c'),
(4, 'echo date(\"Y\");\r\nPo wykonaniu kodu PHP zostanie wyświetlona aktualna data zawierająca jedynie', 'rok.\r\n', 'dzień.\r\n', 'miesiąc.', 'Żadna ze wskazanych.', 'a'),
(5, 'Który zapis definiuje w języku PHP komentarz wieloliniowy?\r\n', '#', '//', '/* */', '<!-- -->', 'c'),
(6, 'Który zapis definiuje w języku PHP komentarz wieloliniowy?\r\n', '#', '//', '/* */', '<!-- -->', 'c'),
(7, 'Integralność encji w bazie danych zostanie zachowana, jeżeli między innymi\r\n', 'klucz główny będzie zawsze liczbą całkowitą.\r\n', ' każdej kolumnie zostanie przypisany typ danych.', 'dla każdej tabeli zostanie utworzony klucz główny', 'każdy klucz główny będzie miał odpowiadający mu klucz obcy w innej tabeli.', 'c'),
(8, 'Aby przy pomocy zapytania SQL zmodyfikować strukturę istniejącej tabeli, należy zastosować kwerendę\r\n', 'UPDATE', 'INSERT INTO\r\n', 'ALTER TABLE\r\n', 'CREATE TABLE\r\n', 'c'),
(9, 'SELECT AVG(cena) FROM uslugi;\r\nFunkcja agregująca AVG użyta w zapytaniu ma za zadanie\r\n', 'zsumować koszt wszystkich usług.', 'wskazać najwyższą cenę za usługi.', 'policzyć ile jest usług dostępnych w tabeli.', 'obliczyć średnią arytmetyczną cen wszystkich usług.', 'd'),
(10, 'SELECT imie FROM mieszkancy WHERE imie LIKE \'_r%\';\r\nKtóre imiona zastosowaną wybrane w wyniku tego zapytania?\r\n', 'Krzysztof, Krystyna, Romuald', 'Rafał, Rebeka, Renata, Roksana.', 'Gerald, Jarosław, Marek, Tamara.', 'Arleta, Krzysztof, Krystyna, Tristan.', 'd'),
(11, 'Kwerendę SELECT DISTINCT należy zastosować w przypadku, gdy potrzeba wybrać rekordy', ' pogrupowane. ', 'występujące w bazie tylko raz.\r\n', 'posortowane malejąco lub rosnąco.', 'tak, aby w podanej kolumnie nie powtarzały się wartości.', 'd'),
(12, 'Którego typu danych w bazie MySQL należy użyć, aby przechować w jednym polu datę i czas?', 'DATE', 'YEAR', 'BOOLEAN\r\n', 'TIMESTAMP', 'd'),
(13, 'Aby edytować dane w bazie danych można posłużyć się', 'raportem.', ' formularzem.\r\n', 'filtrowaniem.\r\n', 'kwerendą SELECT.', 'b'),
(14, 'Aby usunąć wszystkie rekordy z tabeli należy zastosować kwerendę', 'INSERT INTO\r\n', 'ALTER COLUMN', 'CREATE COLUMN', 'TRUNCATE TABLE', 'b'),
(15, 'ALTER TABLE artykuly MODIFY cena float;\r\nKwerenda ma za zadanie w tabeli artykuly', 'usunąć kolumnę cena typu float.\r\n', 'zmienić typ na float dla kolumny cena.', 'zmienić nazwę kolumny z cena na float.', 'dodać kolumnę cena o typie float, jeśli nie istnieje.', 'b'),
(16, 'W języku SQL, aby zabezpieczyć kwerendę CREATE USER tak, aby nie zostało utworzone konto \r\nw przypadku, gdy już istnieje, można posłużyć się składnią\r\n', 'CREATE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER OR DROP \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n', 'CREATE OR REPLACE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER IF NOT EXISTS \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n', 'd'),
(17, 'GRANT SELECT, INSERT, UPDATE ON klienci TO anna;\r\nZakładając, że użytkownik wcześniej nie miał żadnych praw, polecenie SQL nada użytkownikowi anna prawa \r\njedynie do', 'wybierania, wstawiania oraz aktualizacji danych tabeli o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury tabeli o nazwie klienci.', 'wybierania, wstawiania oraz aktualizacji danych wszystkich tabel w bazie o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury wszystkich tabel w bazie o nazwie klienci.', 'a'),
(18, 'W języku SQL, aby zabezpieczyć kwerendę CREATE USER tak, aby nie zostało utworzone konto \r\nw przypadku, gdy już istnieje, można posłużyć się składnią\r\n', 'CREATE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER OR DROP \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n', 'CREATE OR REPLACE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER IF NOT EXISTS \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n', 'd'),
(19, 'GRANT SELECT, INSERT, UPDATE ON klienci TO anna;\r\nZakładając, że użytkownik wcześniej nie miał żadnych praw, polecenie SQL nada użytkownikowi anna prawa \r\njedynie do', 'wybierania, wstawiania oraz aktualizacji danych tabeli o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury tabeli o nazwie klienci.', 'wybierania, wstawiania oraz aktualizacji danych wszystkich tabel w bazie o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury wszystkich tabel w bazie o nazwie klienci.', 'a'),
(20, 'W języku SQL, aby zabezpieczyć kwerendę CREATE USER tak, aby nie zostało utworzone konto \r\nw przypadku, gdy już istnieje, można posłużyć się składnią\r\n', 'CREATE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER OR DROP \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n', 'CREATE OR REPLACE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER IF NOT EXISTS \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n', 'd'),
(21, 'GRANT SELECT, INSERT, UPDATE ON klienci TO anna;\r\nZakładając, że użytkownik wcześniej nie miał żadnych praw, polecenie SQL nada użytkownikowi anna prawa \r\njedynie do', 'wybierania, wstawiania oraz aktualizacji danych tabeli o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury tabeli o nazwie klienci.', 'wybierania, wstawiania oraz aktualizacji danych wszystkich tabel w bazie o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury wszystkich tabel w bazie o nazwie klienci.', 'a'),
(22, 'Pole insert_id zdefiniowane w bibliotece MySQLi języka PHP może być wykorzystane do ', 'otrzymania id ostatnio wstawionego wiersza.', 'otrzymania kodu błędu, gdy proces wstawiania wiersza się nie powiódł.', 'pobrania najwyższego indeksu bazy, aby po jego inkrementacji wstawić pod niego dane.\r\n', 'pobrania pierwszego wolnego indeksu bazy, tak, aby można było pod nim wstawić nowe dane.', 'a'),
(23, 'Znaczniki HTML <strong> oraz <em> służące do podkreślenia ważności tekstu, pod względem formatowania \r\nsą odpowiednikami znaczników', ' <i> oraz <mark>', ' <u> oraz <sup>', ' <b> oraz <i>', '<b> oraz <u>', 'c'),
(24, 'W języku CSS, należy zdefiniować tło dokumentu jako obraz rys.png. Obraz ma powtarzać się jedynie \r\nw poziomie. Którą definicję należy przypisać selektorowi body?', '{background-image: url(\"rys.png\"); background-repeat: round;}\r\n', '{background-image: url(\"rys.png\"); background-repeat: repeat;}\r\n', '{background-image: url(\"rys.png\"); background-repeat: repeat-x;}\r\n', '{background-image: url(\"rys.png\"); background-repeat: repeat-y;}\r\n', 'c'),
(25, 'W języku CSS zapis selektora p > i { color: red;} oznacza, że kolorem czerwonym zostanie \r\nsformatowany', 'każdy tekst w znaczniku <p> lub każdy tekst w znaczniku <i>', 'każdy tekst w znaczniku <p> za wyjątkiem tych w znaczniku <i>\r\n', 'jedynie ten tekst znacznika <p>, do którego jest przypisana klasa o nazwie i\r\n', 'jedynie ten tekst w znaczniku <i>, który jest umieszczony bezpośrednio wewnątrz znacznika <p>', 'd'),
(26, 'input:focus { background-color: LightGreen; }\r\nW języku CSS zdefiniowano formatowanie dla pola edycyjnego. Tak formatowane pole edycyjne będzie miało \r\njasnozielone tło ', 'jeśli jest to pierwsze wystąpienie tego znacznika w dokumencie.\r\n', 'gdy zostanie wskazane kursorem myszy bez kliknięcia.', 'po kliknięciu myszą w celu zapisania w nim tekstu.', 'w każdym przypadku.', 'c'),
(27, 'Kolorem o barwie niebieskiej jest kolor', '#0000EE', '#EE0000', '#00EE00', '#EE00EE\r\n', 'a'),
(28, 'Którym poleceniem można wyświetlić konfigurację serwera PHP, w tym informację m. in. o: wersji PHP, \r\nsystemie operacyjnym serwera, wartości przedefiniowanych zmiennych?', 'echo(ini_get_all());', 'echo phpversion();\r\n', ' phpcredits();', ' phpinfo();\r\n', 'd'),
(29, 'Za pomocą którego słowa kluczowego deklaruje się zmienną w języku JavaScript?\r\n', 'let', 'new', '$', 'begin', 'a'),
(30, 'Pole lub zbiór pól jednoznacznie identyfikujący każdy pojedynczy wiersz w tabeli w bazie danych to klucz', 'inkrementacyjny.\r\n', 'podstawowy.', ' przestawny.', 'obcy.\r\n', 'b'),
(31, 'W języku SQL, aby zmienić strukturę tabeli, np. poprzez dodanie lub usunięcie kolumny, należy zastosować \r\npolecenie', 'UPDATE', 'TRUNCATE\r\n', 'DROP TABLE', 'ALTER TABLE', 'd'),
(32, 'Atrybut kolumny NOT NULL jest wymagany w przypadku', 'klucza podstawowego.', 'użycia atrybutu DEFAULT.\r\n', 'definicji wszystkich pól tabeli.', 'definicji wszystkich pól typu numerycznego.', 'a'),
(33, 'W bazach danych do prezentacji danych spełniających określone warunki nalezy utworzyć', 'raport.', 'relację.', 'formularz.', 'makropolecenie.', 'd'),
(34, 'Wskaż różnicę pomiędzy poleceniami DROP TABLE i TRUNCATE TABLE.\r\n', 'DROP TABLE usuwa tabelę, a TRUNCATE TABLE modyfikuje w niej dane spełniające \r\nwarunek.\r\n', ' DROP TABLE usuwa tabelę, a TRUNCATE TABLE usuwa wszystkie dane, pozostawiając \r\npustą tabelę.\r\n', 'Obydwa polecenia usuwają jedynie zawartość tabeli, ale tylko polecenie DROP TABLE może \r\nbyć cofnięte.\r\n', 'Obydwa polecenia usuwają tabelę wraz zawartością, ale tylko polecenie TRUNCATE TABLE \r\nmoże być cofnięte', 'b'),
(35, 'Aby nadać użytkownikowi uprawnienia do tabel w bazie danych, należy zastosować polecenie ', 'GRANT', 'SELECT ', 'CREATE', 'ALTER', 'a'),
(36, 'Aby przesłać dane za pomocą funkcji mysqli_query() w skrypcie PHP, który wstawia do bazy danych dane \r\npobrane z formularza ze strony internetowej, jako jednego z parametrów należy użyć kwerendy\r\n', 'INSERT INTO', 'UPDATE\r\n', 'SELECT', 'ALTER', 'a'),
(37, 'Który znacznik należy do znaczników definiujących listy w języku HTML?', '<tr>', '<ul>', '<td>', '<br>', 'b'),
(38, 'Której właściwości CSS należy użyć, aby zdefiniować marginesy wewnętrzne dla elementu?', 'hight ', 'margin', 'padding', 'width', 'c'),
(39, 'Globalne tablice do przechowywania danych o ciastkach i sesjach: $_COOKIE oraz $_SESSION są częścią \r\njęzyka', 'C#\r\n', 'Perl', 'PHP\r\n', 'JavaScript', 'c'),
(40, 'treść pytania xyz', 'xddd', 'xddd', 'xddd', 'xddddddddd', 'a');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `pytanie`
--
ALTER TABLE `pytanie`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `pytanie`
--
ALTER TABLE `pytanie`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- Baza danych: `samochody`
--
CREATE DATABASE IF NOT EXISTS `samochody` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `samochody`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `informacje`
--

CREATE TABLE `informacje` (
  `Grzegorz Graczyk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kupiec`
--

CREATE TABLE `kupiec` (
  `id_kupca` int(11) NOT NULL,
  `imie` varchar(30) DEFAULT NULL,
  `nazwisko` varchar(30) DEFAULT NULL,
  `adres` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `kupiec`
--

INSERT INTO `kupiec` (`id_kupca`, `imie`, `nazwisko`, `adres`) VALUES
(1, 'Jan', 'Kowalski', 'Belchatow'),
(2, 'Andrzej', 'Swedrak', 'Szczercow'),
(3, 'Karol', 'Kucharski', 'Szczercow'),
(4, 'Igor', 'Dobrakowski', 'Rzasnia');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `samochody`
--

CREATE TABLE `samochody` (
  `id_car` int(11) NOT NULL,
  `marka` varchar(20) DEFAULT NULL,
  `opis` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `samochody`
--

INSERT INTO `samochody` (`id_car`, `marka`, `opis`) VALUES
(1, 'Toyota', 'Duzy bia?y samochod z 2020'),
(2, 'Toyota', 'Duzy czarny samochod z 2020'),
(3, 'Audi', 'Duzy czarny samochod z 2015'),
(4, 'Audi', 'Duzy czerwony samochod z 2015'),
(5, 'Audi', 'Duzy czarny samochod z 2018');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sprzedawca`
--

CREATE TABLE `sprzedawca` (
  `id_sprzedawcy` int(11) NOT NULL,
  `imie` varchar(30) DEFAULT NULL,
  `nazwisko` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `sprzedawca`
--

INSERT INTO `sprzedawca` (`id_sprzedawcy`, `imie`, `nazwisko`) VALUES
(1, 'Adam', 'Jedraszek'),
(2, 'Bartek', 'Kowalski'),
(3, 'Adam', 'Kowalski'),
(4, 'Dawid', 'Wesolowski'),
(5, 'Jakub', 'Dziedzic');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `kupiec`
--
ALTER TABLE `kupiec`
  ADD PRIMARY KEY (`id_kupca`);

--
-- Indeksy dla tabeli `samochody`
--
ALTER TABLE `samochody`
  ADD PRIMARY KEY (`id_car`);

--
-- Indeksy dla tabeli `sprzedawca`
--
ALTER TABLE `sprzedawca`
  ADD PRIMARY KEY (`id_sprzedawcy`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `kupiec`
--
ALTER TABLE `kupiec`
  MODIFY `id_kupca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `samochody`
--
ALTER TABLE `samochody`
  MODIFY `id_car` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `sprzedawca`
--
ALTER TABLE `sprzedawca`
  MODIFY `id_sprzedawcy` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Baza danych: `szkola`
--
CREATE DATABASE IF NOT EXISTS `szkola` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `szkola`;
--
-- Baza danych: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `test`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `1`
--

CREATE TABLE `1` (
  `567` int(13) DEFAULT NULL COMMENT 's'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `dvd`
--

CREATE TABLE `dvd` (
  `id_dvd` int(11) NOT NULL,
  `tytul` varchar(100) DEFAULT NULL,
  `gatunek` enum('Horror','Thriller','Komedia','Sensacyjny','Obyczajowy') DEFAULT NULL,
  `rok` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `dvd`
--

INSERT INTO `dvd` (`id_dvd`, `tytul`, `gatunek`, `rok`) VALUES
(1, 'Kondzior', 'Horror', 1990),
(2, 'Robot', '', 1930),
(3, 'Rower', '', 1330),
(4, 'Lis', 'Komedia', 1330),
(5, 'Orzel', 'Thriller', 1380),
(6, 'Orzel', 'Sensacyjny', 1380);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `pytania`
--

CREATE TABLE `pytania` (
  `id` int(10) UNSIGNED NOT NULL,
  `pytanie` text COLLATE utf8mb4_polish_ci NOT NULL,
  `odpA` text COLLATE utf8mb4_polish_ci NOT NULL,
  `odpB` text COLLATE utf8mb4_polish_ci NOT NULL,
  `odpC` text COLLATE utf8mb4_polish_ci NOT NULL,
  `odpD` text COLLATE utf8mb4_polish_ci NOT NULL,
  `prawidlowa_odp` text COLLATE utf8mb4_polish_ci NOT NULL,
  `odp` text COLLATE utf8mb4_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `pytania`
--

INSERT INTO `pytania` (`id`, `pytanie`, `odpA`, `odpB`, `odpC`, `odpD`, `prawidlowa_odp`, `odp`) VALUES
(1, 'Program zapisany w języku PHP ma za zadanie obliczyć średnią pozytywnych ocen ucznia od 2 do 6. \nWarunek wybierania ocen w pętli liczącej średnią powinien zawierać wyrażenie logiczne\n', '$ocena > 2 and $ocena < 6', '$ocena > 2 or $ocena < 6', '$ocena >= 2 or $ocena <= 6', '$ocena >= 2 and $ocena <= 6', 'd', ''),
(2, 'W jaki sposób, stosując język PHP, zapisać w ciasteczku napis znajdujący się w zmiennej dane na czas \r\njednego dnia?', 'setcookie(\"dane\", $dane, 0);', ' setcookie(\"dane\", \"dane\", 0);', ' setcookie(\"dane\", $dane, time());\r\n', ' setcookie(\"dane\", $dane, time() + (3600*24)); ', 'd', ''),
(3, 'if (empty($_POST[\"name\"])) {\r\n$nameErr = \"Name is required\";\r\n} \r\nPrzedstawiony fragment kodu PHP służy do obsługi\r\n', 'sesji.', 'ciasteczek.', 'formularza.', 'bazy danych.', 'c', ''),
(4, 'echo date(\"Y\");\r\nPo wykonaniu kodu PHP zostanie wyświetlona aktualna data zawierająca jedynie', 'rok.\r\n', 'dzień.\r\n', 'miesiąc.', 'Żadna ze wskazanych.', 'a', ''),
(5, 'Który zapis definiuje w języku PHP komentarz wieloliniowy?\n', '#', '//', '/* */', '<!-- -->', 'c', ''),
(6, 'Integralność encji w bazie danych zostanie zachowana, jeżeli między innymi\n', 'klucz główny będzie zawsze liczbą całkowitą.\r\n', ' każdej kolumnie zostanie przypisany typ danych.', 'dla każdej tabeli zostanie utworzony klucz główny', 'każdy klucz główny będzie miał odpowiadający mu klucz obcy w innej tabeli.', 'c', ''),
(7, 'Aby przy pomocy zapytania SQL zmodyfikować strukturę istniejącej tabeli, należy zastosować kwerendę\r\n', 'UPDATE', 'INSERT INTO\r\n', 'ALTER TABLE\r\n', 'CREATE TABLE\r\n', 'c', ''),
(8, 'SELECT AVG(cena) FROM uslugi;\r\nFunkcja agregująca AVG użyta w zapytaniu ma za zadanie\r\n', 'zsumować koszt wszystkich usług.', 'wskazać najwyższą cenę za usługi.', 'policzyć ile jest usług dostępnych w tabeli.', 'obliczyć średnią arytmetyczną cen wszystkich usług.', 'd', ''),
(9, 'SELECT imie FROM mieszkancy WHERE imie LIKE \'_r%\';\nKtóre imiona zastosowaną wybrane w wyniku tego zapytania?\n', 'Krzysztof, Krystyna, Romuald', 'Rafał, Rebeka, Renata, Roksana.', 'Gerald, Jarosław, Marek, Tamara.', 'Arleta, Krzysztof, Krystyna, Tristan.', 'd', ''),
(10, 'Kwerendę SELECT DISTINCT należy zastosować w przypadku, gdy potrzeba wybrać rekordy', ' pogrupowane. ', 'występujące w bazie tylko raz.\r\n', 'posortowane malejąco lub rosnąco.', 'tak, aby w podanej kolumnie nie powtarzały się wartości.', 'd', ''),
(11, 'Którego typu danych w bazie MySQL należy użyć, aby przechować w jednym polu datę i czas?', 'DATE', 'YEAR', 'BOOLEAN\r\n', 'TIMESTAMP', 'd', ''),
(12, 'Aby edytować dane w bazie danych można posłużyć się', 'raportem.', ' formularzem.\r\n', 'filtrowaniem.\r\n', 'kwerendą SELECT.', 'b', ''),
(13, 'Aby usunąć wszystkie rekordy z tabeli należy zastosować kwerendę', 'INSERT INTO\r\n', 'ALTER COLUMN', 'CREATE COLUMN', 'TRUNCATE TABLE', 'b', ''),
(14, 'ALTER TABLE artykuly MODIFY cena float;\r\nKwerenda ma za zadanie w tabeli artykuly', 'usunąć kolumnę cena typu float.\r\n', 'zmienić typ na float dla kolumny cena.', 'zmienić nazwę kolumny z cena na float.', 'dodać kolumnę cena o typie float, jeśli nie istnieje.', 'b', ''),
(15, 'W języku SQL, aby zabezpieczyć kwerendę CREATE USER tak, aby nie zostało utworzone konto \r\nw przypadku, gdy już istnieje, można posłużyć się składnią\r\n', 'CREATE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER OR DROP \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n', 'CREATE OR REPLACE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER IF NOT EXISTS \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n', 'd', ''),
(16, 'GRANT SELECT, INSERT, UPDATE ON klienci TO anna;\r\nZakładając, że użytkownik wcześniej nie miał żadnych praw, polecenie SQL nada użytkownikowi anna prawa \r\njedynie do', 'wybierania, wstawiania oraz aktualizacji danych tabeli o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury tabeli o nazwie klienci.', 'wybierania, wstawiania oraz aktualizacji danych wszystkich tabel w bazie o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury wszystkich tabel w bazie o nazwie klienci.', 'a', ''),
(17, 'W języku SQL, aby zabezpieczyć kwerendę CREATE USER tak, aby nie zostało utworzone konto \r\nw przypadku, gdy już istnieje, można posłużyć się składnią\r\n', 'CREATE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER OR DROP \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n', 'CREATE OR REPLACE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER IF NOT EXISTS \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n', 'd', ''),
(18, 'GRANT SELECT, INSERT, UPDATE ON klienci TO anna;\r\nZakładając, że użytkownik wcześniej nie miał żadnych praw, polecenie SQL nada użytkownikowi anna prawa \r\njedynie do', 'wybierania, wstawiania oraz aktualizacji danych tabeli o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury tabeli o nazwie klienci.', 'wybierania, wstawiania oraz aktualizacji danych wszystkich tabel w bazie o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury wszystkich tabel w bazie o nazwie klienci.', 'a', ''),
(19, 'W języku SQL, aby zabezpieczyć kwerendę CREATE USER tak, aby nie zostało utworzone konto \r\nw przypadku, gdy już istnieje, można posłużyć się składnią\r\n', 'CREATE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER OR DROP \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n', 'CREATE OR REPLACE USER \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';', 'CREATE USER IF NOT EXISTS \'anna\'@\'localhost\' IDENTIFIED BY \'yu&T%\';\r\n', 'd', ''),
(20, 'GRANT SELECT, INSERT, UPDATE ON klienci TO anna;\r\nZakładając, że użytkownik wcześniej nie miał żadnych praw, polecenie SQL nada użytkownikowi anna prawa \r\njedynie do', 'wybierania, wstawiania oraz aktualizacji danych tabeli o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury tabeli o nazwie klienci.', 'wybierania, wstawiania oraz aktualizacji danych wszystkich tabel w bazie o nazwie klienci.', 'wybierania, dodawania pól oraz zmiany struktury wszystkich tabel w bazie o nazwie klienci.', 'a', ''),
(21, 'Pole insert_id zdefiniowane w bibliotece MySQLi języka PHP może być wykorzystane do ', 'otrzymania id ostatnio wstawionego wiersza.', 'otrzymania kodu błędu, gdy proces wstawiania wiersza się nie powiódł.', 'pobrania najwyższego indeksu bazy, aby po jego inkrementacji wstawić pod niego dane.\r\n', 'pobrania pierwszego wolnego indeksu bazy, tak, aby można było pod nim wstawić nowe dane.', 'a', ''),
(22, 'Znaczniki HTML <strong> oraz <em> służące do podkreślenia ważności tekstu, pod względem formatowania \r\nsą odpowiednikami znaczników', ' <i> oraz <mark>', ' <u> oraz <sup>', ' <b> oraz <i>', '<b> oraz <u>', 'c', ''),
(23, 'W języku CSS, należy zdefiniować tło dokumentu jako obraz rys.png. Obraz ma powtarzać się jedynie \r\nw poziomie. Którą definicję należy przypisać selektorowi body?', '{background-image: url(\"rys.png\"); background-repeat: round;}\r\n', '{background-image: url(\"rys.png\"); background-repeat: repeat;}\r\n', '{background-image: url(\"rys.png\"); background-repeat: repeat-x;}\r\n', '{background-image: url(\"rys.png\"); background-repeat: repeat-y;}\r\n', 'c', ''),
(24, 'W języku CSS zapis selektora p > i { color: red;} oznacza, że kolorem czerwonym zostanie \r\nsformatowany', 'każdy tekst w znaczniku <p> lub każdy tekst w znaczniku <i>', 'każdy tekst w znaczniku <p> za wyjątkiem tych w znaczniku <i>\r\n', 'jedynie ten tekst znacznika <p>, do którego jest przypisana klasa o nazwie i\r\n', 'jedynie ten tekst w znaczniku <i>, który jest umieszczony bezpośrednio wewnątrz znacznika <p>', 'd', ''),
(25, 'input:focus { background-color: LightGreen; }\r\nW języku CSS zdefiniowano formatowanie dla pola edycyjnego. Tak formatowane pole edycyjne będzie miało \r\njasnozielone tło ', 'jeśli jest to pierwsze wystąpienie tego znacznika w dokumencie.\r\n', 'gdy zostanie wskazane kursorem myszy bez kliknięcia.', 'po kliknięciu myszą w celu zapisania w nim tekstu.', 'w każdym przypadku.', 'c', ''),
(26, 'Kolorem o barwie niebieskiej jest kolor', '#0000EE', '#EE0000', '#00EE00', '#EE00EE\r\n', 'a', ''),
(27, 'Którym poleceniem można wyświetlić konfigurację serwera PHP, w tym informację m. in. o: wersji PHP, \r\nsystemie operacyjnym serwera, wartości przedefiniowanych zmiennych?', 'echo(ini_get_all());', 'echo phpversion();\r\n', ' phpcredits();', ' phpinfo();\r\n', 'd', ''),
(28, 'Za pomocą którego słowa kluczowego deklaruje się zmienną w języku JavaScript?\r\n', 'let', 'new', '$', 'begin', 'a', ''),
(29, 'Pole lub zbiór pól jednoznacznie identyfikujący każdy pojedynczy wiersz w tabeli w bazie danych to klucz', 'inkrementacyjny.\r\n', 'podstawowy.', ' przestawny.', 'obcy.\r\n', 'b', ''),
(30, 'W języku SQL, aby zmienić strukturę tabeli, np. poprzez dodanie lub usunięcie kolumny, należy zastosować \r\npolecenie', 'UPDATE', 'TRUNCATE\r\n', 'DROP TABLE', 'ALTER TABLE', 'd', ''),
(31, 'Atrybut kolumny NOT NULL jest wymagany w przypadku', 'klucza podstawowego.', 'użycia atrybutu DEFAULT.\r\n', 'definicji wszystkich pól tabeli.', 'definicji wszystkich pól typu numerycznego.', 'a', ''),
(32, 'W bazach danych do prezentacji danych spełniających określone warunki nalezy utworzyć', 'raport.', 'relację.', 'formularz.', 'makropolecenie.', 'd', ''),
(33, 'Wskaż różnicę pomiędzy poleceniami DROP TABLE i TRUNCATE TABLE.\r\n', 'DROP TABLE usuwa tabelę, a TRUNCATE TABLE modyfikuje w niej dane spełniające \r\nwarunek.\r\n', ' DROP TABLE usuwa tabelę, a TRUNCATE TABLE usuwa wszystkie dane, pozostawiając \r\npustą tabelę.\r\n', 'Obydwa polecenia usuwają jedynie zawartość tabeli, ale tylko polecenie DROP TABLE może \r\nbyć cofnięte.\r\n', 'Obydwa polecenia usuwają tabelę wraz zawartością, ale tylko polecenie TRUNCATE TABLE \r\nmoże być cofnięte', 'b', ''),
(34, 'Aby nadać użytkownikowi uprawnienia do tabel w bazie danych, należy zastosować polecenie ', 'GRANT', 'SELECT ', 'CREATE', 'ALTER', 'a', ''),
(35, 'Aby przesłać dane za pomocą funkcji mysqli_query() w skrypcie PHP, który wstawia do bazy danych dane \r\npobrane z formularza ze strony internetowej, jako jednego z parametrów należy użyć kwerendy\r\n', 'INSERT INTO', 'UPDATE\r\n', 'SELECT', 'ALTER', 'a', ''),
(36, 'Który znacznik należy do znaczników definiujących listy w języku HTML?', '<tr>', '<ul>', '<td>', '<br>', 'b', ''),
(37, 'Której właściwości CSS należy użyć, aby zdefiniować marginesy wewnętrzne dla elementu?', 'hight ', 'margin', 'padding', 'width', 'c', ''),
(38, 'Globalne tablice do przechowywania danych o ciastkach i sesjach: $_COOKIE oraz $_SESSION są częścią \r\njęzyka', 'C#\r\n', 'Perl', 'PHP\r\n', 'JavaScript', 'c', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `samochody`
--

CREATE TABLE `samochody` (
  `id_samochod` int(11) NOT NULL,
  `marka` varchar(30) DEFAULT NULL,
  `model` varchar(30) DEFAULT NULL,
  `rocznik` int(11) DEFAULT NULL,
  `pojemnosc` int(11) DEFAULT NULL,
  `przyspieszenie` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `samochody`
--

INSERT INTO `samochody` (`id_samochod`, `marka`, `model`, `rocznik`, `pojemnosc`, `przyspieszenie`) VALUES
(1, 'audi', 'e-tron', 2020, 2000, 10),
(2, 'audi', 'e-tron', 2017, 1500, 10),
(3, 'audi', 'e-tron-S', 2021, 2200, 20),
(4, 'opel', 'red-blue', 2021, 2000, 20),
(5, 'opel', 'red-green', 2020, 2000, 20),
(6, 'opel', 'red-yellow', 2019, 2000, 20),
(7, 'opel', 'red-black', 2017, 1800, 15),
(8, 'scoda', 'one-alfa', 2017, 1800, 15),
(9, 'scoda', 'one-beta', 2019, 2500, 20),
(10, 'scoda', 'one-gamma', 2021, 3000, 50);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uprawnienia`
--

CREATE TABLE `uprawnienia` (
  `id_uzytkownika` int(11) DEFAULT NULL,
  `uprawnienia` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `uprawnienia`
--

INSERT INTO `uprawnienia` (`id_uzytkownika`, `uprawnienia`) VALUES
(1, 1),
(2, 1),
(3, 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `ID` int(10) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_polish_ci NOT NULL,
  `password` varchar(200) COLLATE utf8mb4_polish_ci NOT NULL,
  `prawa` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`ID`, `name`, `email`, `password`, `prawa`) VALUES
(10, '', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', '', 1),
(11, '', 'da39a3ee5e6b4b0d3255bfef95601890afd80709', '', 1),
(12, 'wedre', '19187dc98dce52fa4c4e8e05b341a9b77a51fd26', '', 1),
(13, 'wedre', '77de68daecd823babbb58edb1c8e14d7106e83bb', '213', 1),
(14, '2313', '7248feaa77e873d31d864b5cf5b4d35a779d07b9', '123213', 1),
(15, '2313', '58e6b3a414a1e090dfc6029add0f3555ccba127f', '123213', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uzytkownicy`
--

CREATE TABLE `uzytkownicy` (
  `id` int(10) NOT NULL,
  `login` varchar(255) NOT NULL,
  `haslo` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `rejestracja` date NOT NULL,
  `logowanie` date NOT NULL,
  `ip_serwera` varchar(15) NOT NULL,
  `ip1` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `uzytkownicy`
--

INSERT INTO `uzytkownicy` (`id`, `login`, `haslo`, `email`, `rejestracja`, `logowanie`, `ip_serwera`, `ip1`) VALUES
(1, 'admin', '207023ccb44feb4d7dadca005ce29a64', 'admin@admin.pl', '2021-10-20', '2021-10-20', '127.0.0.1', NULL),
(2, 'admin1', 'trudnehaslo', 'admin1@admin1.pl', '2021-10-20', '2021-10-20', '127.0.0.1', NULL),
(3, 'admin2', 'skrtttt', 'admin2@admin2.pl', '2021-10-20', '2021-10-20', '127.0.0.1', NULL),
(4, 'admin3', 'trudne3haslo', 'admin3@admin3.pl', '2021-10-20', '2021-10-20', '127.0.0.1', NULL);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `dvd`
--
ALTER TABLE `dvd`
  ADD PRIMARY KEY (`id_dvd`);

--
-- Indeksy dla tabeli `pytania`
--
ALTER TABLE `pytania`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `samochody`
--
ALTER TABLE `samochody`
  ADD PRIMARY KEY (`id_samochod`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- Indeksy dla tabeli `uzytkownicy`
--
ALTER TABLE `uzytkownicy`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `dvd`
--
ALTER TABLE `dvd`
  MODIFY `id_dvd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `pytania`
--
ALTER TABLE `pytania`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=377;

--
-- AUTO_INCREMENT dla tabeli `samochody`
--
ALTER TABLE `samochody`
  MODIFY `id_samochod` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT dla tabeli `uzytkownicy`
--
ALTER TABLE `uzytkownicy`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Baza danych: `wedkowanie`
--
CREATE DATABASE IF NOT EXISTS `wedkowanie` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `wedkowanie`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `lowisko`
--

CREATE TABLE `lowisko` (
  `id` int(10) UNSIGNED NOT NULL,
  `Ryby_id` int(10) UNSIGNED NOT NULL,
  `akwen` text DEFAULT NULL,
  `wojewodztwo` text DEFAULT NULL,
  `rodzaj` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `lowisko`
--

INSERT INTO `lowisko` (`id`, `Ryby_id`, `akwen`, `wojewodztwo`, `rodzaj`) VALUES
(1, 2, 'Zalew Wegrowski', 'Mazowieckie', 4),
(2, 3, 'Zbiornik Bukowka', 'Dolnoslaskie', 2),
(3, 2, 'Jeziorko Bartbetowskie', 'Warminsko-Mazurskie', 2),
(4, 1, 'Warta-Obrzycko', 'Wielkopolskie', 3),
(5, 2, 'Stawy Milkow', 'Podkarpackie', 5),
(6, 7, 'Przemsza k. Okradzinowa', 'Slaskie', 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `okres_ochronny`
--

CREATE TABLE `okres_ochronny` (
  `id` int(10) UNSIGNED NOT NULL,
  `Ryby_id` int(10) UNSIGNED NOT NULL,
  `od_miesiaca` int(10) UNSIGNED DEFAULT NULL,
  `do_miesiaca` int(10) UNSIGNED DEFAULT NULL,
  `wymiar_ochronny` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `okres_ochronny`
--

INSERT INTO `okres_ochronny` (`id`, `Ryby_id`, `od_miesiaca`, `do_miesiaca`, `wymiar_ochronny`) VALUES
(1, 1, 1, 4, 50),
(2, 2, 0, 0, 30),
(3, 3, 1, 5, 50),
(4, 4, 0, 0, 15),
(5, 5, 11, 6, 70),
(6, 6, 0, 0, 0),
(7, 7, 0, 0, 0),
(8, 8, 0, 0, 25);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ryby`
--

CREATE TABLE `ryby` (
  `id` int(10) UNSIGNED NOT NULL,
  `nazwa` text DEFAULT NULL,
  `wystepowanie` text DEFAULT NULL,
  `styl_zycia` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `ryby`
--

INSERT INTO `ryby` (`id`, `nazwa`, `wystepowanie`, `styl_zycia`) VALUES
(1, 'Szczupak', 'stawy, rzeki', 1),
(2, 'Karp', 'stawy, jeziora', 2),
(3, 'Sandacz', 'stawy, jeziora, rzeki', 1),
(4, 'Okon', 'rzeki', 1),
(5, 'Sum', 'jeziora, rzeki', 1),
(6, 'Dorsz', 'morza, oceany', 1),
(7, 'Leszcz', 'jeziora', 2),
(8, 'Lin', 'jeziora', 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `samochody`
--

CREATE TABLE `samochody` (
  `id` int(10) UNSIGNED NOT NULL,
  `marka` text DEFAULT NULL,
  `model` text DEFAULT NULL,
  `rocznik` year(4) DEFAULT NULL,
  `kolor` text DEFAULT NULL,
  `stan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `samochody`
--

INSERT INTO `samochody` (`id`, `marka`, `model`, `rocznik`, `kolor`, `stan`) VALUES
(1, 'Fiat', 'Punto', 2016, 'czerwony', 'bardzo dobry'),
(2, 'Fiat', 'Punto', 2002, 'czerwony', 'dobry'),
(3, 'Fiat', 'Punto', 2007, 'niebieski', 'bardzo bobry'),
(4, 'Opel', 'Corsa', 2016, 'grafitowy', 'bardzo dobry'),
(5, 'Opel', 'Astra', 2003, 'niebieski', 'porysowany lakier'),
(6, 'Toyota', 'Corolla', 2016, 'czerwony', 'bardzo dobry'),
(7, 'Toyota', 'Corolla', 2014, 'szary', 'dobry'),
(8, 'Toyota', 'Yaris', 2004, 'granatowy', 'dobry');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `typy`
--

CREATE TABLE `typy` (
  `id` int(10) UNSIGNED NOT NULL,
  `kategoria` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `typy`
--

INSERT INTO `typy` (`id`, `kategoria`) VALUES
(1, 'Procesor'),
(2, 'RAM'),
(5, 'karta graficzna'),
(6, 'HDD');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uczniowie`
--

CREATE TABLE `uczniowie` (
  `imie` text DEFAULT NULL,
  `nazwisko` text DEFAULT NULL,
  `wiek` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `uczniowie`
--

INSERT INTO `uczniowie` (`imie`, `nazwisko`, `wiek`) VALUES
('Kamil', 'Ryba', 11),
('Karolina', 'Witecka', 8),
('Karol', 'Rybacki', 9),
('Marina', 'Damiencka', 9);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uzytkownik`
--

CREATE TABLE `uzytkownik` (
  `id` int(10) UNSIGNED NOT NULL,
  `imie` text DEFAULT NULL,
  `nazwisko` text DEFAULT NULL,
  `telefon` text DEFAULT NULL,
  `email` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `uzytkownik`
--

INSERT INTO `uzytkownik` (`id`, `imie`, `nazwisko`, `telefon`, `email`) VALUES
(1, 'Anna', 'Kowalska', '601601601', 'anna@poczta.pl'),
(2, 'Jan', 'Nowak', '608608608', 'jan@poczta.pl'),
(3, 'Jolanta', 'Jasny', '606606606', 'jolanta@poczta.pl'),
(4, 'qqq', 'www', '345', 'dsfsdklfs@daskl');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wyniki`
--

CREATE TABLE `wyniki` (
  `id` int(10) UNSIGNED NOT NULL,
  `dyscyplina_id` int(10) UNSIGNED NOT NULL,
  `sportowiec_id` int(10) UNSIGNED NOT NULL,
  `wynik` decimal(5,2) DEFAULT NULL,
  `dataUstanowienia` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `wyniki`
--

INSERT INTO `wyniki` (`id`, `dyscyplina_id`, `sportowiec_id`, `wynik`, `dataUstanowienia`) VALUES
(1, 1, 1, '12.40', '2015-10-14'),
(2, 1, 1, '12.00', '2015-10-06'),
(3, 1, 2, '11.80', '2015-10-14'),
(4, 1, 2, '11.90', '2015-10-06'),
(5, 1, 3, '11.50', '2015-10-14'),
(6, 1, 3, '11.56', '2015-10-06'),
(7, 1, 4, '11.70', '2015-10-14'),
(8, 1, 4, '11.67', '2015-10-06'),
(9, 1, 5, '11.30', '2015-10-14'),
(10, 1, 5, '11.52', '2015-10-06'),
(11, 1, 6, '12.10', '2015-10-14'),
(12, 1, 6, '12.00', '2015-10-06'),
(13, 3, 1, '63.00', '2015-11-11'),
(14, 3, 1, '63.60', '2015-10-13'),
(15, 3, 2, '64.00', '2015-11-11'),
(16, 3, 2, '63.60', '2015-10-13'),
(17, 3, 3, '60.00', '2015-11-11'),
(18, 3, 3, '61.60', '2015-10-13'),
(19, 3, 4, '63.50', '2015-11-11'),
(20, 3, 4, '63.60', '2015-10-13'),
(21, 3, 5, '70.00', '2015-10-07'),
(22, 3, 6, '68.00', '2015-10-07');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zamowienia`
--

CREATE TABLE `zamowienia` (
  `id` int(10) UNSIGNED NOT NULL,
  `Samochody_id` int(10) UNSIGNED NOT NULL,
  `Klient` text DEFAULT NULL,
  `telefon` text DEFAULT NULL,
  `dataZam` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `zamowienia`
--

INSERT INTO `zamowienia` (`id`, `Samochody_id`, `Klient`, `telefon`, `dataZam`) VALUES
(1, 3, 'Anna Kowalska', '111222333', '2016-02-15'),
(2, 6, 'Jan Nowakowski', '222111333', '2016-02-15'),
(3, 8, 'Marcin Kolwal', '333111222', '2016-02-15');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `lowisko`
--
ALTER TABLE `lowisko`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `okres_ochronny`
--
ALTER TABLE `okres_ochronny`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `ryby`
--
ALTER TABLE `ryby`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `samochody`
--
ALTER TABLE `samochody`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `typy`
--
ALTER TABLE `typy`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `uzytkownik`
--
ALTER TABLE `uzytkownik`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `wyniki`
--
ALTER TABLE `wyniki`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `zamowienia`
--
ALTER TABLE `zamowienia`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `lowisko`
--
ALTER TABLE `lowisko`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `okres_ochronny`
--
ALTER TABLE `okres_ochronny`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT dla tabeli `ryby`
--
ALTER TABLE `ryby`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT dla tabeli `samochody`
--
ALTER TABLE `samochody`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT dla tabeli `typy`
--
ALTER TABLE `typy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT dla tabeli `uzytkownik`
--
ALTER TABLE `uzytkownik`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `wyniki`
--
ALTER TABLE `wyniki`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT dla tabeli `zamowienia`
--
ALTER TABLE `zamowienia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
